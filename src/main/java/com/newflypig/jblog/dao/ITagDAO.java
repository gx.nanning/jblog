package com.newflypig.jblog.dao;

import java.util.List;

import com.newflypig.jblog.model.Tag;

public interface ITagDAO extends IBaseDAO<Tag>{
	
	/**
	 * 手动构建级联关系
	 * @param tagId
	 * @param articleId
	 */
	void makeConnect(Integer tagId, Integer articleId);

	void cleanConnect(Integer articleId);

	List<String> findAllFromTable();

}
