package com.newflypig.jblog.dao;

import com.newflypig.jblog.model.Archive;

public interface IArchiveDAO extends IBaseDAO<Archive>{
	/**
	 * 删除所有数据
	 * 在重新整理归档数据时使用，其余情况不可使用
	 */
	public void clearAll();
}
