package com.newflypig.jblog.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.newflypig.jblog.dao.IBaseDAO;
import com.newflypig.jblog.dao.IWeightDAO;
import com.newflypig.jblog.model.Weight;
import com.newflypig.jblog.service.IWeightService;

@Service("weightService")
public class IWeightServiceImpl extends BaseServiceImpl<Weight> implements IWeightService{
	
	@Autowired
	private IWeightDAO weightDao;
	
	@Override
	protected IBaseDAO<Weight> getDao() {
		// TODO Auto-generated method stub
		return this.weightDao;
	}

}
