package com.newflypig.jblog.service;

import java.util.List;

import com.newflypig.jblog.model.Menu;

public interface IMenuService extends IBaseService<Menu>{
	
	Menu findByUrlName(String urlName);
	
	/**
	 * 根据一组titles查询menus
	 * @param menuTitleList
	 * @return
	 */
	List<Menu> findByTitles(String[] menuTitleList);
	
	/**
	 * 为sitemap提供urls列表，只需要检索简单的title
	 * @return
	 */
	List<String> findMenuUrls();

}
