![JBlog图标](http://ocsy1jhmk.bkt.clouddn.com/f1500c4f-6dda-4105-8261-0440c79cffee.png)

#系统配置在 www.hexcode.cn 服务器上面
#JBlog所开发的点点滴滴也会写成博文发表在上面，欢迎大家访问

[JBlog](http://git.oschina.net/newflydd/jblog) 是一个基于JAVA

适合[JAVA](http://www.oracle.com/technetwork/java/javase/downloads/index.html) 初、中级水平编程人员使用、阅读并修改源代码的个人博客系统

后端技术目前使用了 [Spring](http://spring.io/)，[Hibernate](http://hibernate.org/)，[SpringMVC](http://projects.spring.io/spring-framework/)，[Freemarker](http://freemarker.org/)

项目管理使用了 [gradle](https://gradle.org/)

数据库使用了[MySQL](https://www.mysql.com/)

登陆验证时使用了 [极验(Geetest)](http://www.geetest.com/), [Spring Security](http://projects.spring.io/spring-security/)

前端技术目前使用了 [jQuery](http://jquery.com/)，[AmazeUI](http://amazeui.org/)， [Editor.md](https://pandao.github.io/editor.md/) 并为其添加了上传图片到 「[七牛](https://www.qiniu.com/)」 的插件，

计划内的还会使用 [Lucene](http://lucene.apache.org/) 做全文检索，[Quartz](http://www.quartz-scheduler.org/) 做服务器端计划任务（数据库备份之类的）

说实话以上每个带有超链的URL都够初学者学习一阵子了，还不包括HTML5，CSS3，JavaScript ES6 等

以后还会引入更多更好玩的前后端的技术

希望跟有共同兴趣爱好的朋友结识

配的服务器在 [www.hexcode.cn](http://www.hexcode.cn) 上面（登陆入口[www.hexcode.cn/login](http://www.hexcode.cn/login)），登陆界面大家可以体验，已经开放来宾帐户，用户名guest，密码guest，来宾进入后台可以在后台界面闲逛，浏览后台的「控制台」UI，但不可以有实质性操作。同时诚邀精通黑客技术的朋友们来测试登陆入口，本人并不擅长这些黑科技，如果有什么漏洞或者BUG，也请善意的通知我，在此先谢过！

作者争取每天都会写一些代码提交，如有任何想法或者问题，欢迎跟作者联系: <newflydd@189.cn>

##Fork提示
自从「码云」把我这个小blog项目推荐了以后，渐渐有朋友来主页上star和watch了，这也敦促了我坚持把这个小项目做好做美。
还有一些朋友勇敢的Fork了我的项目，可惜我知道即使Fork回去也没法使用的，因为我做了一个磁盘文件jblog.token，文件里面有1024个字符用来DES加密和解密数据库地址和密码，如果Fork回去只能先看看代码了，没有磁盘密钥解密是运行不了的。
我给数据库添加了一个只有对jblog库有select权限的用户，Fork代码的朋友可以使用这个来宾帐户进行数据源的配置，至少能把项目在tomcat或者其他容器上跑起来。为了保证我的远程数据库性能不受影响，来宾的数据库帐户每小时查询数我限制在1800，平均每2秒查询一次，但有些关联查询操作会多一些。有特殊需求的朋友可以联系我 <newflydd@189.cn>
修改`/src/main/java/config.porperties`文件：
```
#下面这个随便写一个磁盘文件路径，别让他报错即可，这是我私有的密钥，其他朋友用不到
jblog.token.filepath = c:/jblog.token
#jdbc druid config
jdbc.url = jdbc:mysql://121.42.170.67:3306/jblog2?useUnicode=true&characterEncoding=utf-8
jdbc.username = jblog_guest
jdbc.password = jblog_guest
```
对我是如何加密`config.properties`文件感兴趣的朋友可以阅读我的一篇博文：[基于Spring的JAVA开源项目如何保护隐私数据](http://www.hexcode.cn/article/show/encrypt-properties)
同时，我也导出了一份最新SQL文件，给大家自行导入到本地服务器：[SQL的下载地址](http://ocsy1jhmk.bkt.clouddn.com/ece5987f-b446-4c74-ba07-bc331eac3535.sql)。

##使用准备
如果您像我一样喜欢使用大量第三方成熟、稳定的轮子，您可以尝试使用JBlog
所以在您使用之前，希望您除了JAVA技术方向的知识储备以外，还需要拥有以下服务账号：
- [七牛云存储](http://www.qiniu.com)（第三方云存储，用来保存文章中的图片，去中心化，节省自建服务器的流量，降低负载，博客搬家时也会轻松许多）
- [多说评论](http://www.duoshuo.com)（评论列表就不要自己造轮子了，毕竟与文章的一对多关系，再加上自身还有个一对多的父嵌套，还是挺复杂的）
- [Geetest极验](http://www.geetest.com/)（人机识别，虽然我们的系统没多少商业价值，但安全性依然很重要，防爆破，防脚本。放弃使用字符验证码吧，七歪八扭的字符真的会降低用户体验的，如果你自己每天登陆的话，自己都会被自己丑死）

以上所有帐号都是免费注册的，作者也没在这些服务上花钱买VIP服务，都是使用的最基本的免费服务。使用数据库来宾帐号登陆我的数据库的朋友，能够看到这些帐号的HASH令牌数据，但是恳请各位对这个小项目感兴趣，Fork代码的朋友，动动手自己注册一个帐号，以便更好地使用这些云服务。不要把文件上传到我的七牛空间，我这边一旦在控制台删除你是恢复不了的；不要用我的多说ID，那样在你们的系统上评论都到我这边来了；也不要用我的geetest帐号，会导致一些安全问题。

##近期计划：
- ~~给系统添加生成RSS的功能~~(2016年9月18日20:43:48 完成)
- ~~首页添加「返回顶部」的阻尼滚动按钮~~(2016年9月19日22:29:41 完成)
- ~~添加来宾帐户用来浏览后台界面~~（2016年9月23日08:10:29）
- ~~将所有资源文件（js,css,fonts）放到七牛CDN服务器加速下载，避免每个用户请求时访问本地服务器下载这些资源文件，优先放置fonts文件，js和css等逐渐稳定后再放~~(2016年9月29日22:06:43 完成)
- 给系统配置英文版字符库，实现国际化
- ~~后台实现右边栏插件的管理功能~~(2016年9月27日17:03:38 完成)
- 添加一个插件，通过 [番茄土豆](https://pomotodo.com/) 的API读取作者目前正在处理的标签为 #jblog 的事务
- 后台实现自定义查询，对ID，urlName，日期，标题，tag，menu，内容字符等进行条件查询，比如要修改一篇标题包含「ABC」的文章，但现在文章已经很多了，翻页非常不方便，此时就需要自定义查询快速定位。
- 写一个爬虫，抓取 http://my.csdn.net/my/mycsdn 页面上关于编程的小tip，放到数据库中，以供自己的应用使用，可以做一个右边栏插件随机显示
- 引入 [Lucene](http://lucene.apache.org/) 做站内检索
- 引入 [Quartz](http://www.quartz-scheduler.org/) 做服务器端计划任务（定时备份数据库、提交URL给搜索引擎之类的）


##更新日志：
###2016年9月29日22:06:55
适配了nginx，并在服务器上成功部署，使用nginx的重定向功能，将指向`/resources/*`路径的URL全部转向到了七牛CDN，同时使用七牛的同步软件，将resources文件夹同步在七牛CDN中。这样，用户请求的所有CSS，js以及fonts字体，全部由第三方云服务负责传输，降低了自身服务器的负载，节省了流量开支。详细操作步骤可以参考我的博文：[使用Nginx配合免费的七牛帐号打造自己的CDN加速服务](http://www.hexcode.cn/article/show/nginx-cdn)

###2016年9月27日16:57:19
后台添加插件管理功能，右边栏Widget的添加、删除、排序、修改等功能，使用editormd的HTML模式进行编辑，同时开放了`script`,`style`,`iframe`三大特殊标签的直接录入。所以在editormd中输入任何js代码或者CSS样式，是实时生效的，并且在编辑时就会有反馈，所以需要合理运用。暂时添加了GIT@OSC的一个小挂件，显示JBlog项目的在GIT@OSC上的一些基本情况。

###2016年9月24日08:06:00
添加来宾帐号，用户名密码均为「guest」。使用SpringMVC的拦截技术，对`/admin/**`和`/**/admin/**`通配URL进行POST请求的拦截，如果是来宾帐号则提示权限不够，如果是正式管理员则放行。关于SpringMVC和Spring Security近期我会写一篇文章来介绍一下配置。还是有点复杂的，有机会试试Shiro。

###2016年9月19日21:39:14
添加了「返回顶部」功能，在首页和文章页，只要页面滚轮脱离顶部100px，则在右下方显示「返回顶部」的图标按钮，点击后在800毫秒内页面返回顶部，并隐藏图标按钮，看上去有一种阻尼效果，还是挺满意的。
详细设计方法可以阅读：[页面中「返回顶部」图标按钮的实现](http://www.hexcode.cn/article/show/back-to-top)

###2016年9月18日17:35:09
添加了自动生成RSS功能，并通过了[http://www.feedvalidator.org/](http://www.feedvalidator.org/)的RSS校验
系统将拦截URL为`/admin/article/**`或者`/article/admin/**`通配的路径，在所有的POST请求之后，抛出一个线程，重构根目录下的rss.xml文件，这样的生成策略保证了rss.xml对系统的性能影响最小，不用每次响应用户的RSS请求时，对数据库进行大面积扫描，又保证了每次管理员增、删、改文章时（这些操作一般使用POST方式提交），会生成一个最新的RSS文件，同时，使用后台抛出线程的方式，避免了用户在前台执行这些增删改操作后，WEB不能及时响应，影响用户体验的问题。
具体实现方法可以阅读： [为JBlog添加RSS订阅功能](http://www.hexcode.cn/article/show/generate-rss-with-java)